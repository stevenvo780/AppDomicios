import * as React from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { createOrderAction } from '../../../store/reducer';
import {
  Container,
  Col,
  Form,
  FormGroup,
  Input,
  Button,
  Label,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from 'reactstrap';
import Select from 'react-select';

// Departments And Cities JSON
import { cities, departments } from './lib/cities';

import moment from 'moment';

// Take Order Component
const TakeOrder = () => {
  const { companyName, idCompnay, deliveryNumber, purchaseNumber } =
    useParams();

  // Redux Dispatch
  const dispatch = useDispatch();

  // Get Current User
  const user = useSelector((state) => state.login.user);

  const [clientPhone, setClientPhone] = React.useState();
  const [pickupAddress, setPickupAddress] = React.useState();
  const [deliveryAddress, setDeliveryAddress] = React.useState();
  const [department, setDepartment] = React.useState({});
  const [city, setCity] = React.useState({});
  const [neighbourhood, setNeighbourhood] = React.useState();
  const [residentialGroupName, setResidentialGroupName] =
    React.useState();
  const [houseNumberOrApartment, setHouseNumberOrApartment] =
    React.useState();
  const [deliveryNote, setDeliveryNote] = React.useState();
  const [deliveryPacket, setDeliveryPacket] = React.useState();
  const [paymentMethod, setPaymentMethod] = React.useState({});
  const [names, setNames] = React.useState();
  const [lastnames, setLastNames] = React.useState();
  const [documentNumber, setDocumentNumber] = React.useState();
  const [typeDocument, setTypeDocument] = React.useState({
    value: 'cc',
    label: 'Cedula',
  });
  const [email, setEmail] = React.useState();

  const [toggle, setToggle] = React.useState(false);
  const [formIsValid, setFormIsValid] = React.useState(false);

  // Handle names change
  const handleNamesChange = (names) => {
    setLastNames(names.target.value);
  };

  // Handle last names change
  const handleLastNamesChange = (lastNames) => {
    setNames(lastNames.target.value);
  };

  // Handle Type Documnet Change
  const handleTypeDocumentChange = (role) => {
    setTypeDocument(role);
  };

  // Handle document number change
  const handleDocumentChange = (document) => {
    setDocumentNumber(document.target.value);
  };

  // Handle Phone Change
  const handlePhoneChange = (phone) => {
    setClientPhone(phone.target.value);
  };

  // Handle Pick Up Address
  const handlePickupAddress = (pickupAddress) => {
    setPickupAddress(pickupAddress.target.value);
  };

  // Handle Delivery Address
  const handleDeliveryAddress = (deliveryAddress) => {
    setDeliveryAddress(deliveryAddress.target.value);
  };

  // Hanlde the event onChange to multi select department
  const handleDepartmentChange = (departmentData, index) => {
    department[index] = departmentData;
    setDepartment(departmentData);
  };

  const handleCityChange = (cityData, index) => {
    city[index] = cityData;
    setCity(city);
  };

  // Handle Neighbourgood change
  const handleNeighborhoodChange = (neighbourhood) => {
    setNeighbourhood(neighbourhood.target.value);
  };

  // Handle Residential Group Name
  const handleResidentialGroupNameChange = (residentialGroupName) => {
    setResidentialGroupName(residentialGroupName.target.value);
  };

  // Handle House Or Apartment Number
  const handleHouseOrApartmentChange = (houseOrApartmentNumber) => {
    setHouseNumberOrApartment(houseOrApartmentNumber.target.value);
  };

  // Handle Delivery Note Change
  const handleDeliveryNoteChange = (deliveryNote) => {
    setDeliveryNote(deliveryNote.target.value);
  };

  // Handle Delivery Note Change
  const handleDeliveryPacket = (deliveryPacket) => {
    setDeliveryPacket(deliveryPacket.target.value);
  };

  // Hanlde event onChange to payMethod
  const handePayMethodChange = (paymentData, index) => {
    paymentMethod[index] = paymentData;
    setPaymentMethod(paymentData);
  };

  // Handle Email Change
  const handleEmailChange = (email) => {
    setEmail(email.target.value);
  };

  // Get departments simple info
  const departmentSimpleInfo = [];
  let number = 0;
  Object.keys(department).forEach((key) => {
    departmentSimpleInfo[number] = department[key];
    number++;
  });

  // Get id of department
  const idDepartment = [];
  for (let i = 0; i < departments.length; i++) {
    if (departments[i].departamento === department.label)
      idDepartment.push(departments[i].id);
  }

  // Get cities data
  const getCities = cities.map((city) => {
    let citie = {};
    if (city.id == departmentSimpleInfo[0]) {
      citie = city.ciudades.map((citi) => {
        return {
          value: city.id,
          label: citi,
        };
      });
    }
    return citie;
  });

  let finalCitiesData = [];
  getCities.map((citie) => {
    if (citie.length > 1) {
      finalCitiesData.push(citie);
    }
  });

  let finalInfoCitiesData = [
    {
      value: 3,
      label: 'Medellín',
    },
    {
      value: 4,
      label: 'Bello',
    },
  ];

  let temporalfinalInfoCitiesData = finalCitiesData.map(
    (finalData) => {
      let object = finalData.map((final) => {
        return {
          value: Math.floor(Math.random() * 100),
          label: final.label,
        };
      });
      return object.map((obj) => {
        return obj;
      });
    }
  );

  if (temporalfinalInfoCitiesData.length > 0) {
    finalInfoCitiesData = temporalfinalInfoCitiesData;
  }

  // Payment Methods
  const paymentMethods = [
    {
      method: 'Efectivo',
      id: Math.floor(Math.random() * 100),
    },
    {
      method: 'Transferencia Bancaria',
      id: Math.floor(Math.random() * 100),
    },
  ];

  // Handle Close
  const handleToggle = (e) => {
    e.preventDefault();
    setToggle(!toggle);
  };

  // Register WhatsApp
  const openWhatsapp = () => {
    const url = process.env.REACT_APP_URL_WHATSAPP;
    window.open(url, '_blank');
  };

  // Handle Save
  const handleSave = () => {
    let data = {
      deliveryNumber: Number(deliveryNumber),
      purchaseNumber: Number(purchaseNumber),
      creationDate: moment(Date.now()).format('YYYY/MM/DD'),
      clientPhone: clientPhone,
      pickupAddress: pickupAddress,
      department: department.label,
      departmentId: department.value,
      city: city.label || 'Default city',
      neighbourhood: neighbourhood,
      residentialGroupName: residentialGroupName,
      houseNumberOrApartment: houseNumberOrApartment,
      deliveryAddress: deliveryAddress,
      deliveryNote: deliveryNote,
      deliveryPacket: deliveryPacket,
      orderState: 'waiting',
      paymentMethod: paymentMethod.label.toString(),
      company: companyName,
      companyId: Number(idCompnay),
      names: names,
      lastnames: lastnames,
      documentNumber: documentNumber,
      email: email,
    };
    dispatch(createOrderAction(data));
    console.log('What data im gonna send', data);
  };

  React.useEffect(() => {
    if (
      formIsValid === false &&
      Object.keys(department).length &&
      Object.keys(city).length &&
      Object.keys(paymentMethod).length
    ) {
      setFormIsValid(true);
    }
  }, [
    setFormIsValid,
    formIsValid,
    deliveryNumber,
    purchaseNumber,
    clientPhone,
    pickupAddress,
    department,
    city,
    neighbourhood,
    residentialGroupName,
    houseNumberOrApartment,
    deliveryNote,
    deliveryPacket,
    paymentMethod,
    names,
    lastnames,
    documentNumber,
    email,
  ]);

  return (
    <>
      <div>
        <Container
          className="themed-container containerProof"
          fluid="sm"
        >
          <FormGroup>
            <div className="positionButton">
              <Button
                variant="success"
                size="sm"
                onClick={openWhatsapp}
              >
                Registrar WhatsApp
              </Button>{' '}
              {``}
            </div>
          </FormGroup>
          <Form className="form" onSubmit={(e) => handleToggle(e)}>
            <h2 className="takeOrderTitle">Tomar Orden</h2>

            <Col>
              <FormGroup row>
                <Col sm={10}>
                  <Input
                    required
                    type="text"
                    id="names"
                    placeholder="Nombres"
                    onChange={handleNamesChange}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={10}>
                  <Input
                    required
                    type="text"
                    id="lastNames"
                    placeholder="Apellidos"
                    onChange={handleLastNamesChange}
                  />
                </Col>
              </FormGroup>
              <FormGroup>
                <Label for="examplePassword">Tipo de documento</Label>
                <Select
                  onChange={handleTypeDocumentChange}
                  value={typeDocument}
                  placeholder="Tipo de documento"
                  options={[
                    {
                      value: 'cc',
                      label: 'Cedula',
                    },
                    {
                      value: 'nit',
                      label: 'NIT',
                    },
                  ]}
                />
              </FormGroup>
              <FormGroup row>
                <Col sm={10}>
                  <Input
                    required
                    type="text"
                    id="documentNumber"
                    placeholder="Documento"
                    onChange={handleDocumentChange}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={10}>
                  <Input
                    required
                    type="text"
                    id="phone"
                    placeholder="Telefono"
                    onChange={handlePhoneChange}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={10}>
                  <Input
                    required
                    type="text"
                    id="email"
                    placeholder="Email"
                    onChange={handleEmailChange}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={10}>
                  <Input
                    required
                    type="text"
                    id="pickupAddress"
                    placeholder="Dirección Recogida"
                    onChange={handlePickupAddress}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={10}>
                  <Input
                    required
                    type="text"
                    id="phoneNumber"
                    placeholder="Dirección Entrega"
                    onChange={handleDeliveryAddress}
                  />
                </Col>
              </FormGroup>

              <FormGroup row>
                <Col sm={10}>
                  <Select
                    onChange={handleDepartmentChange}
                    placeholder="Departamento"
                    options={departments.map((department) => {
                      return {
                        value: department.id,
                        label: department.departamento,
                      };
                    })}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={10}>
                  <Select
                    required
                    onChange={handleCityChange}
                    placeholder="Ciudad"
                    options={finalInfoCitiesData[0]}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={10}>
                  <Input
                    required
                    type="text"
                    id="phoneNumber"
                    placeholder="Barrio"
                    onChange={handleNeighborhoodChange}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={10}>
                  <Input
                    required
                    type="text"
                    id="phoneNumber"
                    placeholder="Nombre De Conjunto Residencial"
                    onChange={handleResidentialGroupNameChange}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={10}>
                  <Input
                    required
                    type="text"
                    id="phoneNumber"
                    placeholder="Número de Casa O Apartamento"
                    onChange={handleHouseOrApartmentChange}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={10}>
                  <Input
                    required
                    type="text"
                    id="phoneNumber"
                    placeholder="Nota De Entrega"
                    onChange={handleDeliveryNoteChange}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={10}>
                  <Input
                    required
                    type="text"
                    id="phoneNumber"
                    placeholder="Paquete A Entregar"
                    onChange={handleDeliveryPacket}
                  />
                </Col>
              </FormGroup>

              <FormGroup row>
                <Col sm={10}>
                  <Select
                    onChange={handePayMethodChange}
                    placeholder="Método De Pago"
                    options={paymentMethods.map((method) => {
                      return {
                        value: method.id,
                        label: method.method,
                      };
                    })}
                  />
                </Col>
              </FormGroup>
              <FormGroup>
                <div className="positionButton">
                  <Button
                    variant="success"
                    size="lg"
                    type="submit"
                    disabled={!formIsValid}
                  >
                    Crear Orden
                  </Button>{' '}
                  {``}
                </div>
              </FormGroup>
            </Col>
            <Col></Col>
          </Form>
        </Container>
      </div>
      <SaveOrderModal
        toggle={toggle}
        handleChange={handleSave}
        handleClose={handleToggle}
      />
    </>
  );
};

const SaveOrderModal = (props) => {
  const { toggle, handleChange, handleClose } = props;
  return (
    <>
      <Modal isOpen={toggle} toggle={handleChange}>
        <ModalHeader toggle={handleClose}>Confirmar</ModalHeader>
        <ModalBody>
          ¿Estás seguro/a de que los datos ingresados en el formulario
          son correctos?
        </ModalBody>
        <ModalFooter>
          <Button onClick={handleChange}>Aceptar</Button>
          <Button onClick={handleClose}>Cancelar</Button>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default TakeOrder;
